# Snatch Webservice Api V 1.0.2


###Base Url Webservice : 

[Snatch Webservice Link : http://webservice.snatch.fi/v1/main/](http://webservice.snatch.fi/v1/main/)


###Urls List 

##auth needs to be included in every request header. you must receive auth hash value from snatch support.

###We need a url from your side whenever a delivery action is taken. Once a delivery action is taken, we will send you a delivery parameter in get format.
####Delivery parameter is a code which is set specificly for that specific order.

for example your url is : 
http://www.example.com/deliverylistener 

Once delivery action is taken, we webhook an url like this : 
http://www.example.com/deliverylistener?delivery=alsdh3rehiwuebir37&invoiceid=sn-123
 ```
    * Products 
        * details : get a list of all products
        * parameters:
            * page parameter starts from 0
            * the default perpage parameter is 6 for product results
            
        * url : [http://webservice.snatch.fi/v1/main/products] 
        
```
```
    * Product
        * details : get product details
        * parameters:
            * id | product id
        * url : [http://webservice.snatch.fi/v1/main/product] 
         
```
```
* product-check
    * details : check the possibility of product purchase | check for quantity and expiration
        * parameters:
            * id | product id
    * url : [http://webservice.snatch.fi/v1/main/product-check] 
    
```
```
* reserve
    * details : Reserve a product
        * parameters:
            * id | product id
            * quantity | product quantity
            * userid | the userid that you provide in reservation step(From here on, this userid is used repeatedly in following steps)
    * url : [http://webservice.snatch.fi/v1/main/reserve] 
    
```

```
* reserve-delete
    * details : delete a reserve
        * parameters:
            * code  | reserve code that you recieve from us in reserve action
            * userid | the userid that you provide
    * url : [http://webservice.snatch.fi/v1/main/reserve-delete] 
    * return  json format | status 1 or 0
```

```
* finalize
    * details : purchase order is finalized(which is called once a customer pays for a product)
        * parameters:
            * code  | reserve code that you recieve from us in reserve action
            * userid | the userid that you provide
            * invoiceid | the invoiceid that you provide. Must be unique
            * customer_name | your customer's name
    * url : [http://webservice.snatch.fi/v1/main/finalize] 
    * return  json format |invoiceid = your invoiceid ,  code = order code , 
```


```
* invoice
    * details :  invoice information and details
        * parameters:
            * userid | the userid that you provide
            * invoiceid | the invoiceid that you provide. Must be unique
    * url : [http://webservice.snatch.fi/v1/main/invoice] 
    
```


```
* delivery
    * details : when delivery action is taken
        * parameters:
            * userid | the userid that you provide
            * invoiceid | the invoiceid that you provide. Must be unique
            * delivery_code | the delivery_code you recieve from us(webhook)
    * url : [http://webservice.snatch.fi/v1/main/invoice] 
    * return  json format | status 1 or 0
```

