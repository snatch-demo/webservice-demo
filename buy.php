<?php


use libs\snatchApi;

include_once 'autoload.php';
include_once 'vendor/autoload.php';


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="MJ">
    <meta name="generator" content="Snatch ">
    <title>Api Demo · Snatch</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap.min.css" rel="stylesheet">
    <link href="assets/styles.css" rel="stylesheet">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
</head>
<body>
<?php include_once('partial/head.php'); ?>


<main role="main">


    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                <?php


                $api = new snatchApi();

                $productid = (isset($_GET['code'])) ? $_GET['code'] : 0;
                $product = $api->getProduct($productid);


                $check = (isset($_GET['check'])) ? ($_GET['check'] == "true") ?? true : false;

                if ($check) {
                    $check = $api->getProductCheck($productid);

                    ?>

                    <div class="col-md-4 col-12 my-auto flex-col">
                        <div class="card mb-4 shadow-sm flex-col">
                            <div class="card-header"><h6 class="card-title font-weight-bold m-0">Status</h6></div>
                            <div class="card-body">
                                <?php
                                if ($check->quantity == 0 || $check->expired == 1) {
                                    ?>
                                    <div class="text-danger">Product Quantity 0 OR Product Expired</div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="text-success">Can Buy</div>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                    <?php

                }
                ?>

                <div class="col-md-6 col-12 m-auto flex-col">
                    <div class="card mb-4 shadow-sm flex-col">


                        <img src="<?= @$product->photos[0]; ?>" class="prod-img">
                        <div class="card-body">
                            <h3 class="prod-title"><?= $product->title; ?></h3>
                            <h6 class="sub-title text-muted">Category: <?= $product->cat->title; ?></h6>
                            <p class="card-text">
                                <?= strip_tags($product->description); ?>
                            </p>

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item text-dark">
                                    <img src="<?= $product->store->photo ?>" class="store-img">
                                    <?= $product->store->title; ?>
                                </li>

                                <li class="list-group-item text-danger">Old Price:
                                    <del><?= $product->price_old; ?></del>
                                </li>
                                <li class="list-group-item text-info ">Price: <?= $product->price; ?></li>
                                <li class="list-group-item text-info ">Discount: <?= $product->discount; ?> %</li>
                                <li class="list-group-item text-dark ">Quantity: <?= $product->sold; ?>
                                    / <?= $product->quantity; ?></li>

                                <li class="list-group-item text-dark ">Expire
                                    At: <?= date('Y-m-d H:i:s', $product->expire_at); ?></li>
                            </ul>

                            <div class="frm-card mt-2 py-2 px-5 ">
                                <form action="reserve.php" method="get">

                                    <div class="form-group">
                                        <label for="quanitty_inp">Quantity</label>
                                        <select id="quanitty_inp" class="form-control" name="quantity">
                                            <?php

                                            for ($i = 1; $i <= $product->quantity; $i++) {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="userid_inp">Userid</label>
                                        <input  id="userid_inp" class="form-control" name="userid" type="text" value="<?= 'userid'.rand(500,5000); ?>" />
                                        <input  class="form-control" name="code" type="hidden" value="<?= $productid ?>" />
                                    </div>


                                    <button href="buy.php?code=<?= $product->id ?>"
                                       class="btn btn-sm btn-primary btn-block ">Reserve Products</button>

                                </form>

                            </div>

                            <div class="d-flex mt-3 justify-content-between align-items-center">
                                <div class="btn-group">


                                    <a href="buy.php?code=<?= $product->id ?>&check=true"
                                       class="btn btn-sm btn-outline-info ">Check Product</a>
                                </div>
                                <small class="text-muted">userid to this sample is : testapi</small>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>

    </div>

</main>


<script src="assets/jquery-3.3.1.slim.min.js"></script>
<script src="assets/bootstrap.bundle.min.js"></script>
</body>
</html>