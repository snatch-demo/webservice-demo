<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


use libs\snatchApi;

include_once 'autoload.php';
include_once 'vendor/autoload.php';


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="MJ">
    <meta name="generator" content="Snatch ">
    <title>Api Demo · Snatch</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap.min.css" rel="stylesheet">
    <link href="assets/styles.css" rel="stylesheet">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
</head>
<body>
<?php include_once('partial/head.php'); ?>


<main role="main">

    <section class="jumbotron text-center mb-0">
        <div class="container">
            <h1 class="jumbotron-heading">Welcome to Snatch Api</h1>
            <p class="lead text-muted">
                For more information, it's best to read the document.
            </p>
            <p>
<a href="https://gitlab.com/snatch-demo/webservice-demo/blob/master/README.md" class="btn btn-primary my-2">Document</a>
                <a href="https://gitlab.com/snatch-demo/webservice-demo" class="btn btn-secondary my-2">Gitlab Page</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                <?php


                $api = new snatchApi();

$page = (isset($_GET['page'])) ? (int) $_GET['page'] : 0;
                foreach ($api->getProducts($page) as $product) {

                    ?>
                    <div class="col-md-4 flex-col">
                        <div class="card mb-4 shadow-sm flex-col">


                            <img src="<?=$product->photo; ?>"  class="prod-img">
                            <div class="card-body">
                                <h3 class="prod-title"><?= $product->title; ?></h3>
                                <p class="card-text">

                                </p>

                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item text-danger">Old Price: <del><?= $product->price_old; ?></del></li>
                                    <li class="list-group-item text-info ">Price: <?= $product->price; ?></li>
                                    <li class="list-group-item text-dark ">Quantity: <?= $product->sold; ?> / <?= $product->quantity; ?></li>
                                </ul>

                                <div class="d-flex mt-3 justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <a href="buy.php?code=<?= $product->id ?>"  class="btn btn-sm btn-outline-secondary btn-block">buy</a>
                                    </div>
                                    <small class="text-muted">Expire At: <?= date('Y-m-d H:i:s',$product->expire_at); ?></small>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?>
            </div>


            <div class="btn-group mr-2" role="group" aria-label="First group">
                <a  href="?page=0" class="btn btn-secondary">1</a>
                <a href="?page=1" class="btn btn-secondary">2</a>
                <a href="?page=2" class="btn btn-secondary">3</a>
            </div>

        </div>

    </div>

</main>


<script src="assets/jquery-3.3.1.slim.min.js"></script>
<script src="assets/bootstrap.bundle.min.js"></script>
</body>
</html>