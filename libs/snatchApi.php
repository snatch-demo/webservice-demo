<?php

namespace libs;

use Curl\Curl;

class  snatchApi
{


    /**
     * @var string
     * @see api key for snatch
     */
    private $key = "I59uwm9S9fwR3aZ7PD8UzWfdLewrbDKJjV0";

    /**
     * @var string
     * @see base url api
     */
    private $baseUrl = 'http://webservice.snatch.fi/v1/main/';


    /**
     * @var string
     * @see responce if invalid key
     */
    private $invalidKey = "INVALID_KEY";

    public function __construct($key = "")
    {
        if ($key != "")
            $this->key = $key;
    }


    /**
     * @param int $page
     * @return mixed
     * @throws \ErrorException
     * @see get all products
     */
    public function getProducts($page = 0)
    {
        $curl = new Curl();
        $curl->setHeader('auth', $this->key);
        $curl->get($this->baseUrl . "products", ['page' => $page]);

        if ($curl->response == $this->invalidKey)
            die('INVALID API KEY');

        if ($curl->http_status_code != 200)
            die($curl->getErrorMessage());
        $response = json_decode($curl->response);

        if ($response->status != 1)
            die($curl->getErrorMessage());

        return $response->data->products;
    }


    /**
     * @param int $id
     * @return mixed
     * @throws \ErrorException
     * @see get details product
     */
    public function getProduct($id = 0)
    {

        if (strlen($id) < 8)
            die('product code not correct');


        $curl = new Curl();
        $curl->setHeader('auth', $this->key);


        $curl->get($this->baseUrl . "product", ['id' => $id]);

        if ($curl->response == $this->invalidKey)
            die('INVALID API KEY');

        if ($curl->http_status_code != 200)
            die($curl->getErrorMessage());
        $response = json_decode($curl->response);

        if ($response->status != 1)
            die($curl->getErrorMessage());

        return $response->data->product;
    }


    public function getProductCheck($id = 0)
    {

        if (strlen($id) < 8)
            die('product code not correct');


        $curl = new Curl();
        $curl->setHeader('auth', $this->key);
        $curl->get($this->baseUrl . "product-check", ['id' => $id]);

        if ($curl->response == $this->invalidKey)
            die('INVALID API KEY');

        if ($curl->http_status_code != 200)
            die($curl->getErrorMessage());
        $response = json_decode($curl->response);

        if ($response->status != 1)
            die(' ERROR : ' . $curl->getErrorMessage());


        return $response->data->product;
    }


    /**
     * @param int $id
     * @param int $quantity
     * @param string $userid
     * @return mixed
     * @throws \ErrorException
     * @see Reserve Product
     *
     */
    public function reserve($id = 0, $quantity = 1, $userid = "testapi")
    {

        if (strlen($id) < 8)
            die('product code not correct');


        $curl = new Curl();
        $curl->setHeader('auth', $this->key);
        $curl->get($this->baseUrl . "reserve", ['b' => 1, 'id' => $id, 'quantity' => $quantity, 'userid' => $userid]);


        if ($curl->response == $this->invalidKey)
            die('INVALID API KEY');


        if ($curl->http_status_code != 200)
            die($curl->getErrorMessage());


        $response = json_decode($curl->response);

        if ($response->status != 1)
            die(' ERROR : ' . $response->data->msg);

        return $response->data->code;
    }


    /**
     * @param int $id
     * @param int $quantity
     * @param string $userid
     * @return mixed
     * @throws \ErrorException
     * @see Delete Reserve
     */
    public function reserveDelete($id = 0, $userid = "testapi")
    {



        $curl = new Curl();
        $curl->setHeader('auth', $this->key);
        $curl->get($this->baseUrl . "reserve-delete", ['code' => $id, 'userid' => $userid ,'d'=>1]);


        if ($curl->response == $this->invalidKey)
            die('INVALID API KEY');

        $response = json_decode($curl->response);

        if ($response->status != 1)
            die(' ERROR : INVALID Code' );

        header('location: index.php');
    }


    /**
     * @param int $reservecode
     * @param $invoiceid
     * @param $customername
     * @param $userid
     * @throws \ErrorException
     * @see Final purchase order
     */
    public function finalize($reservecode = 0,$invoiceid, $customername ,  $userid )
    {



        $curl = new Curl();
        $curl->setHeader('auth', $this->key);
        $curl->get($this->baseUrl . "finalize", ['code' => $reservecode, 'userid' => $userid ,'customer_name'=>$customername, 'invoiceid'=>$invoiceid]);


        if ($curl->response == $this->invalidKey)
            die('INVALID API KEY');

        $response = json_decode($curl->response);

        if ($response->status != 1)
            die(' ERROR : ' . $response->data->msg );

        header('location: order.php?invoiceid='.$invoiceid.'&cardid='.$response->data->code .'&userid='.$userid.'&code='.$response->data->code);
    }



    /**
     * @param $invoiceid
     * @param $userid
     * @return mixed order data
     * @throws \ErrorException
     * @see Final purchase order
     */
    public function invoice($invoiceid, $userid )
    {
        $curl = new Curl();
        $curl->setHeader('auth', $this->key);
        $curl->get($this->baseUrl . "invoice", [ 'userid' => $userid , 'invoiceid'=>$invoiceid]);


        if ($curl->response == $this->invalidKey)
            die('INVALID API KEY');

        $response = json_decode($curl->response);

        if ($response->status != 1)
            die(' ERROR : ' . $response->data->msg );

        return $response->data->order;
    }

    /**
     * @param $invoiceid
     * @param $userid
     * @return mixed order data
     * @throws \ErrorException
     * @see Final purchase order
     */
    public function delivery($delivery_code,$invoiceid, $userid )
    {
        $curl = new Curl();
        $curl->setHeader('auth', $this->key);
        $curl->get($this->baseUrl . "delivery", [ 'delivery_code'=>$delivery_code,'userid' => $userid , 'delivery_code'=>$invoiceid]);


        if ($curl->response == $this->invalidKey)
            die('INVALID API KEY');

        $response = json_decode($curl->response);

//        if ($response->status != 1)
//            die(' ERROR : ' . $response->data->msg );

        return $response;
    }
}