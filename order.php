<?php


use libs\snatchApi;

include_once 'autoload.php';
include_once 'vendor/autoload.php';


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="MJ">
    <meta name="generator" content="Snatch ">
    <title>Api Demo · Snatch</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap.min.css" rel="stylesheet">
    <link href="assets/styles.css" rel="stylesheet">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
</head>
<body>
<?php include_once('partial/head.php'); ?>


<main role="main">


    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                <?php


                $api = new snatchApi();

                $invoiceid = (isset($_GET['invoiceid'])) ? $_GET['invoiceid'] : 0;
                $code = (isset($_GET['code'])) ? $_GET['code'] : 0;
                $userid = (isset($_GET['userid'])) ? $_GET['userid'] : 0;
                $invoice = $api->invoice($invoiceid, $userid);


                ?>

                <div class="col-md-6 col-12 m-auto flex-col">
                    <div class="card mb-4 shadow-sm flex-col">
                        <div class="card-header"><h6 class="card-title text-success font-weight-bold m-0">Order
                                successfully </h6></div>
                        <div class="card-body">

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"> invoiceid : <?= $invoice->invoiceid; ?></li>
                                <li class="list-group-item"> Card code : <?= $invoice->code; ?></li>
                                <li class="list-group-item"> Customerid : <?= $invoice->customerid; ?></li>
                                <li class="list-group-item"> Customer name : <?= $invoice->customername; ?></li>
                                <li class="list-group-item">
                                    Date: <?= date('d.m.Y H:i:s', strtotime($invoice->date)); ?></li>
                                <li class="list-group-item text-dark">
                                    <img src="<?= $invoice->store->photo ?>" class="store-img">
                                    <?= $invoice->store->title; ?>
                                </li>

                                <li class="list-group-item text-dark">
                                    Store Address:
                                    <?= $invoice->store->address; ?>
                                </li>
                                <li class="list-group-item text-dark">
                                    Store Phone:
                                    <?= $invoice->store->phone; ?>
                                </li>

                                <li class="list-group-item text-dark">
                                    <img src="<?= $invoice->photo ?>" class="prod-img2">
                                    <?= $invoice->title; ?>
                                </li>
                                <li class="list-group-item text-danger">Old Price:
                                    <del><?= $invoice->priceBase; ?></del>
                                </li>
                                <li class="list-group-item text-info ">Price: <?= $invoice->price; ?></li>
                                <li class="list-group-item text-info ">Discount: <?= $invoice->discount; ?> %</li>
                                <li class="list-group-item  ">Delivery
                                    : <?= ($invoice->delivery_status == 0) ? " <span class='text-danger'>NO DELIVERY </span>" : "<span class='text-danger'>delivered </span>"; ?></li>

                            </ul>


                            <div class="p-3">

                                <div class="frm-card mt-2 py-2 px-5">
                                    <?php


                                    if(isset($_POST['delivery_code']) && $_POST['userid']){

                                        $delivery_code = $_POST['delivery_code'];
                                        $delivery_userid = $_POST['userid'];
                                        $delivery_invoiceid = $_POST['invoiceid'];
                                        $result = $api->delivery($delivery_code,$delivery_invoiceid,$delivery_userid);

                                        if($result->status == 0){
                                            echo '<div class="alert alert-danger">Invalid Parameter Send</div>';
                                        }else{
                                            echo '<div class="alert alert-success">Successfully order delivered</div>';
                                        }

                                    }
                                    ?>
                                    <form action="" method="post">
                                        <p> if receive delivery code from webhook enter code and final delivery .✌</p>

                                        <div class="form-group">
                                            <label for="userid_inp">delivery code</label>
                                            <input id="userid_inp" class="form-control" name="delivery_code" type="text"
                                                   value=""/>


                                        </div>

                                        <div class="form-group">
                                            <label for="userid_inp">Userid</label>
                                            <input class="form-control" name="userid"
                                                   value="<?= $invoice->customerid ?>"/>


                                        </div>


                                        <div class="form-group">
                                            <label for="userid_inp">invoiceid</label>
                                            <input class="form-control" name="invoiceid"
                                                   value="<?= $invoice->invoiceid ?>"/>


                                        </div>




                                        <button
                                                class="btn btn-sm btn-primary btn-block ">Reserve Products
                                        </button>

                                    </form>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>


        </div>

    </div>

</main>


<script src="assets/jquery-3.3.1.slim.min.js"></script>
<script src="assets/bootstrap.bundle.min.js"></script>
</body>
</html>