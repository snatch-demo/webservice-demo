<?php


use libs\snatchApi;

include_once 'autoload.php';
include_once 'vendor/autoload.php';


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="MJ">
    <meta name="generator" content="Snatch ">
    <title>Api Demo · Snatch</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap.min.css" rel="stylesheet">
    <link href="assets/styles.css" rel="stylesheet">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
</head>
<body>
<?php include_once('partial/head.php'); ?>


<main role="main">


    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                <?php


                $api = new snatchApi();

                $productid = (isset($_GET['code'])) ? $_GET['code'] : 0;
                $userid = (isset($_GET['userid'])) ? $_GET['userid'] : 0;
                $quantity = (isset($_GET['quantity'])) ? $_GET['quantity'] : 0;
                $reserve = $api->reserve($productid, $quantity, $userid);
                $product = $api->getProduct($productid);


                ?>

                <div class="col-md-4 col-12 my-auto flex-col">
                    <div class="card mb-4 shadow-sm flex-col">
                        <div class="card-header"><h6 class="card-title font-weight-bold m-0">Status</h6></div>
                        <div class="card-body">

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"> Reserve Code : <?= $reserve; ?></li>
                                <li class="list-group-item"> Quantity : <?= $quantity; ?></li>
                                <li class="list-group-item"> UseRId: <?= $userid; ?></li>
                            </ul>

                            <div class="p-3">

                                <div class="btn-group">


                                    <a href="finalize.php?code=<?= $reserve ?>&invoiceid=<?= 'API-'.rand(1000,999999); ?>&customer_name=API ORDER CUSTOMER&userid=<?= $userid ; ?>"
                                       class="btn btn-sm btn-outline-info ">Final purchase order</a>
                                    <a href="reservedelete.php?code=<?= $product->id ?>&reservecode=<?= $reserve; ?>&userid=<?= $userid ; ?>"
                                       class="btn btn-sm btn-outline-danger ">Delete Reserve</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="col-md-5 col-12 m-auto flex-col">
                    <div class="card mb-4 shadow-sm flex-col">


                        <img src="<?= @$product->photos[0]; ?>" class="prod-img">
                        <div class="card-body">
                            <h3 class="prod-title"><?= $product->title; ?></h3>
                            <h6 class="sub-title text-muted">Category: <?= $product->cat->title; ?></h6>
                            <p class="card-text">
                                <?= strip_tags($product->description); ?>
                            </p>

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item text-dark">
                                    <img src="<?= $product->store->photo ?>" class="store-img">
                                    <?= $product->store->title; ?>
                                </li>

                                <li class="list-group-item text-danger">Old Price:
                                    <del><?= $product->price_old; ?></del>
                                </li>
                                <li class="list-group-item text-info ">Price: <?= $product->price; ?></li>
                                <li class="list-group-item text-info ">Discount: <?= $product->discount; ?> %</li>
                                <li class="list-group-item text-dark ">Quantity: <?= $product->sold; ?>
                                    / <?= $product->quantity; ?></li>

                                <li class="list-group-item text-dark ">Expire
                                    At: <?= date('Y-m-d H:i:s', $product->expire_at); ?></li>
                            </ul>




                        </div>
                    </div>
                </div>

            </div>


        </div>

    </div>

</main>


<script src="assets/jquery-3.3.1.slim.min.js"></script>
<script src="assets/bootstrap.bundle.min.js"></script>
</body>
</html>